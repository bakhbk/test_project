import 'package:flutter/material.dart';

class CTextEditingController extends TextEditingController {
  CTextEditingController({
    required this.maxAllowedValue,
    double initialValue = 0,
    this.decimalSeparator = '',
    this.thousandSeparator = '.',
    this.rightSymbol = '',
    this.leftSymbol = '',
    this.precision = 0,
  }) {
    _validateConfig();
    addListener(updateValue);
    if (initialValue != 0) updateValue(initialValue);
  }

  final String decimalSeparator;
  final String thousandSeparator;
  final String rightSymbol;
  final String leftSymbol;
  final int precision;
  final double maxAllowedValue;

  double _lastValue = 0.0;

  void updateValue([double? _value]) {
    final value = _value ?? numberValue;
    if (value >= maxAllowedValue) {
      final _lastValueSting = _lastValue.toInt().toString();
      text = _lastValueSting;
      selection = TextSelection.fromPosition(
        TextPosition(
          offset: _lastValueSting.length,
        ),
      );
      return;
    }

    double valueToUse = value;

    if (value.toStringAsFixed(0).length > 12) {
      valueToUse = _lastValue;
    } else {
      _lastValue = value;
    }

    String masked = _applyMask(valueToUse);

    if (rightSymbol.isNotEmpty) {
      masked += rightSymbol;
    }

    if (leftSymbol.isNotEmpty) {
      masked = leftSymbol + masked;
    }

    if (masked != text) {
      text = masked;

      final cursorPosition = super.text.length - rightSymbol.length;
      selection = TextSelection.fromPosition(
        TextPosition(
          offset: cursorPosition,
        ),
      );
    }
  }

  double get numberValue {
    final List<String> parts =
        _getOnlyNumbers(text).split('').toList(growable: true);

    parts.insert(parts.length - precision, '.');

    try {
      return double.parse(parts.join());
    } catch (e) {
      return 0;
    }
  }

  void _validateConfig() {
    final rightSymbolHasNumbers = _getOnlyNumbers(rightSymbol).isNotEmpty;

    if (rightSymbolHasNumbers) {
      throw ArgumentError("rightSymbol must not have numbers.");
    }
  }

  String _getOnlyNumbers(String text) {
    return text.replaceAll(RegExp(r'[^\d]'), '');
  }

  String _applyMask(double value) {
    final textRepresentation = value
        .toStringAsFixed(precision)
        .replaceAll('.', '')
        .split('')
        .reversed
        .toList(growable: true);

    textRepresentation.insert(precision, decimalSeparator);

    for (var i = precision + 4; true; i = i + 4) {
      if (textRepresentation.length > i) {
        textRepresentation.insert(i, thousandSeparator);
      } else {
        break;
      }
    }

    return textRepresentation.reversed.join();
  }
}
