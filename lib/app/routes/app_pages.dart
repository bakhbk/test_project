import 'package:get/get.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/item/bindings/item_binding.dart';
import '../modules/home/item/views/item_view.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
      children: [
        GetPage(
          name: _Paths.ITEM,
          page: () => ItemView(),
          binding: ItemBinding(),
        ),
      ],
    ),
  ];
}
