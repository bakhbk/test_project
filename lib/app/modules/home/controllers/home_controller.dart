import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:test_project/app/routes/app_pages.dart';

abstract class HomeKeys {
  static const itemCount = 'itemCount';
  static const currentIndex = 'currentIndex';
}

class HomeController extends GetxController {
  static const _unselectedItem = -1;
  final scrollController = ScrollController();
  final itemScrollController = ItemScrollController();
  late final AnimationController animationController;
  int itemCount = 16;
  double angle = 0.0;
  RxInt selectedItem = _unselectedItem.obs;

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(_scrollControllerListener);
  }

  @override
  void onClose() {
    scrollController.dispose();
    animationController.dispose();
    super.onClose();
  }

  Future<void> openItemViewPage(int index) async {
    Get.closeAllSnackbars();
    Get.toNamed(
      Routes.ITEM,
      arguments: {
        HomeKeys.itemCount: itemCount,
        HomeKeys.currentIndex: index,
      },
    );
    selectedItem.trigger(_unselectedItem);
  }

  void showSavedItem(int selectedIndex) {
    selectedItem.trigger(selectedIndex);
    itemScrollController.scrollTo(
      index: selectedIndex,
      duration: const Duration(seconds: 1),
      curve: Curves.easeInOutCubic,
    );
  }

  void _scrollControllerListener() {
    animationController.value = angle = -(scrollController.offset / 100);
    _showSnackBarIfNeeded();
  }

  void _showSnackBarIfNeeded() {
    final maxOffset = scrollController.position.maxScrollExtent;
    final currentOffset = scrollController.position.pixels;
    if (currentOffset == maxOffset) {
      Get.showSnackbar(
        const GetSnackBar(
          title: 'Attention',
          message: 'You have reached the top of the screen!',
          duration: Duration(milliseconds: 1500),
        ),
      );
    }
  }
}
