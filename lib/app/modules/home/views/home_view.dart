import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:test_project/app/modules/home/controllers/home_controller.dart';

part 'components/custom_scroll_view.dart';

part 'components/horizontal_list_view.dart';

part 'components/icon_view.dart';

part 'components/rows.dart';

class HomeView extends StatelessWidget {
  const HomeView();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: const [
          _IconView(),
          _CustomScrollView(),
        ],
      ),
    );
  }
}
