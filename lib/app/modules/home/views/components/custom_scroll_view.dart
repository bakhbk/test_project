part of '../home_view.dart';

class _CustomScrollView extends GetView<HomeController> {
  const _CustomScrollView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return CustomScrollView(
      controller: controller.scrollController,
      slivers: [
        SliverAppBar(
          backgroundColor: Colors.transparent,
          expandedHeight: MediaQuery.of(context).size.height / 3.5,
          flexibleSpace: const FlexibleSpaceBar(),
        ),
        SliverFillRemaining(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: themeData.bottomAppBarColor,
              boxShadow: [
                BoxShadow(
                  color: themeData.shadowColor,
                  blurRadius: 3.0,
                  offset: const Offset(0, 1),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: const [
                SizedBox(height: 36),
                _Rows(),
                _HorizontalListView(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
