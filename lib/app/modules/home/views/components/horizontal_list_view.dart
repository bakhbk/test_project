part of '../home_view.dart';

class _HorizontalListView extends GetView<HomeController> {
  const _HorizontalListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Flexible(
      child: ScrollablePositionedList.separated(
        padding: const EdgeInsets.symmetric(
          horizontal: 24,
          vertical: 48,
        ),
        itemCount: controller.itemCount,
        shrinkWrap: true,
        itemScrollController: controller.itemScrollController,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, index) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: GestureDetector(
                  onTap: () => controller.openItemViewPage(index),
                  child: SizedBox(
                    width: 84.0,
                    height: 84.0,
                    child: Obx(
                      () => DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: controller.selectedItem.value == index
                              ? themeData.colorScheme.primary
                              : themeData.scaffoldBackgroundColor,
                          boxShadow: [
                            BoxShadow(
                              color: themeData.shadowColor,
                              blurRadius: 3.0,
                              offset: const Offset(0, 1),
                            ),
                          ],
                        ),
                        child: const SizedBox.expand(),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
        separatorBuilder: (_, __) => const SizedBox(width: 24),
      ),
    );
  }
}
