part of '../home_view.dart';

class _Rows extends StatelessWidget {
  const _Rows({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: const [
        _Row(),
        SizedBox(height: 32),
        _Row(rightPadding: 36),
        SizedBox(height: 32),
        _Row(rightPadding: 124),
      ],
    );
  }
}

class _Row extends StatelessWidget {
  const _Row({Key? key, this.rightPadding = 92}) : super(key: key);

  final double rightPadding;

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Row(
      children: [
        const SizedBox(width: 24),
        Expanded(
          child: Container(
            height: 56,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: themeData.shadowColor,
                  blurRadius: 3.0,
                  offset: const Offset(0, 1),
                ),
              ],
              borderRadius: BorderRadius.circular(36),
              gradient: LinearGradient(
                colors: [
                  themeData.secondaryHeaderColor,
                  themeData.scaffoldBackgroundColor,
                ],
              ),
            ),
          ),
        ),
        SizedBox(width: rightPadding),
      ],
    );
  }
}
