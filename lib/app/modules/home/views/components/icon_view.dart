part of '../home_view.dart';

class _IconView extends GetView<HomeController> {
  const _IconView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Positioned(
      left: -200,
      top: -200,
      child: _RotateIcon(),
    );
  }
}

class _RotateIcon extends StatefulWidget {
  const _RotateIcon({Key? key}) : super(key: key);

  @override
  State<_RotateIcon> createState() => _RotateIconState();
}

class _RotateIconState extends State<_RotateIcon>
    with SingleTickerProviderStateMixin {
  final homeController = Get.find<HomeController>();

  @override
  void initState() {
    super.initState();
    homeController.animationController = AnimationController(
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: homeController.animationController,
      builder: (_, child) => Transform.rotate(
        angle: homeController.angle,
        child: child,
      ),
      child: SvgPicture.asset(
        'assets/icons/circle.svg',
        color: Theme.of(context).colorScheme.onSurface,
        width: 400,
        height: 400,
      ),
    );
  }
}
