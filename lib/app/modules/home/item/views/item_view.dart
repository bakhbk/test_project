import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_project/app/modules/home/item/controllers/item_controller.dart';

class ItemView extends GetView<ItemController> {
  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Text('Allowed values are from 0 to ${controller.maxLength - 1}'),
            TextField(
              autofocus: true,
              keyboardType: TextInputType.number,
              controller: controller.textController,
            ),
            const Expanded(child: SizedBox()),
            TextButton(
              onPressed: controller.save,
              style: TextButton.styleFrom(
                backgroundColor: themeData.colorScheme.background,
              ),
              child: const Text('Save'),
            )
          ],
        ),
      ),
    );
  }
}
