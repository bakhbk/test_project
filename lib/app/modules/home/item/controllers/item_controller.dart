import 'package:get/get.dart';
import 'package:test_project/app/helpers/custom_text_editing_controller.dart';
import 'package:test_project/app/modules/home/controllers/home_controller.dart';

class ItemController extends GetxController {
  late final CTextEditingController textController;
  late final int maxLength;
  late final int currentIndex;

  @override
  void onInit() {
    super.onInit();
    final arguments = Get.arguments;
    if (arguments is Map<String, dynamic>) {
      maxLength = arguments[HomeKeys.itemCount] as int? ?? 0;
      currentIndex = arguments[HomeKeys.currentIndex] as int? ?? 0;
      textController = CTextEditingController(
        maxAllowedValue: maxLength.toDouble(),
        initialValue: currentIndex.toDouble(),
      );
    }
  }

  @override
  void onClose() {
    textController.dispose();
  }

  void save() {
    Get.find<HomeController>().showSavedItem(_parseInt());
    Get.back();
  }

  int _parseInt() {
    try {
      return int.parse(textController.text);
    } on FormatException catch (_) {
      return 0;
    }
  }
}
